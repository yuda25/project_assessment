<?php
include "connection.php";
include "function.php";

if(isset($_POST['submit'])){

    inputData($_POST);
    header('location:index.php');
}

if(isset($_GET['delete'])){
    deleteData($_GET);
    header('location:index.php');
}

if(isset($_POST['ganti'])){
    updateData($_POST['nama'],$_POST['asal'],$_POST['harga'],$_POST['id']);
    header('location:index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        .rounded-image{
	        width: 100px;
	        height: 100px;
	        background: #dac52c;
	        border-radius: 100%;
        }
        body{
            font-family: sans-serif; 
            background-color: #f1c40f;
	        -webkit-animation: color 5s ease-in  0s infinite alternate running;
	        -moz-animation: color 5s linear  0s infinite alternate running;
	        animation: color 5s linear  0s infinite alternate running;
        }
        /* Animasi + Prefix untuk browser */
        @-webkit-keyframes color {
            12% { background-color: #00ff00; }
            25% { background-color: #b039bb; }
            50% { background-color: #4a4ad6; }
            75% { background-color: #3af50b; }
            100% { background-color: #f50b0b; }
        };
        @-moz-keyframes color {
            12% { background-color: #00ff00; }
            25% { background-color: #b039bb; }
            50% { background-color: #4a4ad6; }
            75% { background-color: #3af50b; }
            100% { background-color: #f50b0b; }
        };
        @keyframes color {
            12% { background-color: #00ff00; }
            25% { background-color: #b039bb; }
            50% { background-color: #4a4ad6; }
            75% { background-color: #3af50b; }
            100% { background-color: #f50b0b; }
        };
        .navbar{
            color: black;
        }
        #NAV{
            background: linear-gradient(to right, orange, #ec38bc, #7303c0, cyan);
            font-weight: bold;
            font-family: fantasy;
            font-size: 19px;
            margin-top: 0px;
            height: 60px;
        }
        .copyright{
            margin-left: 40%;
        } 
        .card{
            background-image: url('img/bacground.jpg');
            margin-left: 25px;
            margin-top: 30px;
        }
    </style>
    <link rel="shortcut icon" type="image/png" href="img/logo.png"/>
    <title>♛ʏᴜᴅᴀ ɢᴀᴍɪɴɢ sᴛᴏʀᴇ♛</title>
</head>
<body>
<!-- NAV BAR -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top scrolling-navbar" id="NAV">
<nav class="navbar navbar-dark">
    <p class="navbar">
        <img src="img/logo.png" width="40" height="40" class="d-inline-block align-top" alt="logo" loading="lazy">
        <b style="color: black;"><i>YUDA GAMING STORE</i></b>
    </p>
</nav>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">HOME<span class="sr-only">(current)</span></a>
            </li>
                <li class="nav-item">
            <a class="nav-link" href="#add">ADD</a>
            </li>
                <li class="nav-item">
                <a class="nav-link" href="#list">LIST</a>
            </li>
                <li class="nav-item">
                <a class="nav-link" href="#about">ABOUT</a>
            </li>
            <li class="nav-item">
                 <a class="nav-link" href="#footer">CONTACT</a>
            </li>
        </ul>
    </div>
</nav>

<!-- CAROUSEL -->
<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="img/carousel1.jpg" class="d-block w-100" alt="satu">
      <div class="carousel-caption d-none d-md-block">
        <h5>Pengiriman</h5>
        <p>Kami melakukan pengiriman barang dengan secepat mungkin, batas maksimal 3h++ maka uang akan di kemblikan.</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/carousel2.jpg" class="d-block w-100" alt="dua">
      <div class="carousel-caption d-none d-md-block">
        <h5>Kualitas</h5>
        <p>Barang yang di jual dapat di pastikan barang yang dijual awet, tahan lama dan ber kualitas.</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/carousel3.jpg" class="d-block w-100" alt="tiga">
      <div class="carousel-caption d-none d-md-block">
        <h5>Keamanan</h5>
        <p>Kami pastikan barang yang di jual di sini adalah barang yang ori.</p>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<!-- CARD -->
<div class="container mt-5">
    <div class="row">
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Pengiriman</h5>
                    <p class="card-text">Kami melakukan pengiriman barang dengan secepat mungkin, batas maksimal 3h++ maka uang akan di kemblikan.</p>
                    <i class="fas fa-car"></i>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Kualitas</h5>
                    <p class="card-text">Barang yang di jual dapat di pastikan barang yang dijual awet, tahan lama dan ber kualitas.</p>
                    <br>
                    <i class="fas fa-medal"></i>
                </div>
            </div>
        </div>
        <di class="col-4">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Keamanan</h5>
                    <p class="card-text">Kami pastikan barang yang di jual di sini adalah barang yang ori.</p>
                    <br>
                    <br>
                    <i class="fas fa-lock"></i>
                </div>
            </div>
        </di>
    </div>
</div>

<!-- FORM -->
<div class="container">
    <div class="row mt-5">
        <div class="col">
            <form action="index.php" method="POST">
                <div class="form-group">
                    <h3 id="add"><b>ADD YOUR ITEM</b></h3>
                    <label>Brand</label>
                    <input type="text" name="nama" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>From City</label>
                    <input type="text" name="asal" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Price</label>
                    <input type="text" name="harga" class="form-control" required>
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Add  <i class="far fa-plus-square"></i></button>
            </form>
        </div>
    </div>
</div>

<!-- tabel -->
<div class="container"> 
    <div class="row mt-5">
        <div class="col" id="list">
            <h3><b>LIST ITEM</b></h3>
            <table border="1" class="table table-striped table-hover dtabel">
                <thead class="thead-dark">
                    <tr>
                        <th>Brand</th>
                        <th>From City</th>
                        <th>Price</th>
                        <th>option</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data_toko as $key):?>
                    <tr>
                        <td><?php echo $key["nama"]; ?></td>
                        <td><?php echo $key["asal"]; ?></td>
                        <td><?php echo $key["harga"]; ?></td>
                        <td><a onclick="runPopup()" class="btn btn-danger" href="index.php?delete=&id=<?php echo $key['id'];?>">DELETE  <i class="far fa-trash-alt"></i></a>
                        <a class="btn btn-success" href="edit.php?id=<?php echo $key['id'];?>">EDIT  <i class="far fa-edit"></i></a> </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- ABOUT US -->
<div class="container">
    <div class="row">
        <div class="col-12">
            <center><b><h1 id="about">♕the best gaming store♕</h1></b></center>
            <p>Yuda Gaming Store is a gaming store located in Tangerang, Green Lake City, CMD M-59 which was founded by Pro Gamers in Indonesia,
            We improve ourselves in answering your gaming needs. Our store supplies leading Gaming equipment from various countries that have been 
            proven in the world of gamers. Our mission is very simple, to propose the gaming industry in Indonesia by providing the best gaming 
            equipment. We not only sell goods, we understand what we sell, we review gaming goods that end with our community, we are able to provide 
            your gaming gear services too, and of course we provide the best after-sales service, especially in after-sales issues and all guarantees 
            are only to answer your gaming gear needs, ask your colleagues who already live in their Yuda Gaming Shop, they must have experienced our 
             best after sales.</p>
            <br>
            <br>
            <i>translate</i>
            <p>Yuda Gaming Store adalah toko gaming yang berlokasi di Tangerang , Green Lake City, CMD M-59 yang didirikan Oleh Pro Gamers di indonesia ,
            Kita mendedikasikan diri kita dalam menjawab Kebutuhan Gaming anda . Toko kami di supplies dengan Gaming equipment terkemuka dari berbagai
            negara yang telah terbukti di dunia gamers . Misi kita sangat sederhana , memajukan Industri gaming di indonesia dengan cara memberikan 
            gaming gear yang terbaik . Kita tidak hanya bisa menjual barang , kita mengerti apa yang kami jual , kami meng review barang gaming yang
            beredar sendiri dengan komunitas kami , kami mampu mengservice gaming gear anda juga , dan tentunya kita memberikan Service purna jual
            yang terbaik terutama dalam masalah after sales dan garansi semuanya hanya untuk menjawab kebutuhan Gaming Gear anda , bertanyalah 
            kepada rekan anda yang sudah berbelanja di Yuda Gaming Shop mereka pastilah mereka sudah merasakan after sales terbaik kami .</p>
            <center><img class="rounded-image" src="img/saya.jpg" /></center>
            <center><b>⫷Kusuma Yuda Mubarok⫸</b></center>
            <center><i>yuda gaming shop✅</i></center>
        </div>
    </div>
</div>

<!-- FOOTER -->
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <section id="footer">
                <div class="row mt-5 bg-dark text-white">
                    <div class="col-sm-4">
                        <h2 class="display-5 text-center">Location</h2>
                        <!-- maaf tulisannya CBD M-58 karena yang 59 nggak ada -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d897.3922526401655!2d106.69901252917656!3d-6.183870166418676!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f9d8f9215f27%3A0x7a320b90434606f8!2sCBD%20M58%20GLC!5e1!3m2!1sid!2sid!4v1603421107289!5m2!1sid!2sid" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                    <div class="col-sm-4 text-center">
                        <h2 class="display-5 text-center">Contact</h2>
                        <i class="fab fa-whatsapp" style=" font-size:2rem;"></i>
                        <a href="https://wa.me/628980249767" class="text-white text-decoration-none btn btn-outline-light align-top mt-0 ml-1">08980249787</a>
                    </div>
                    <div class="col-sm-4 text-center">
                        <h2 class="display-5 text-center">Sosial media</h2>
                        <i class="fab fa-facebook-square" style="font-size:2rem;"></i>
                        <a href="https://web.facebook.com/kusuma.yuda.750/" class="text-white text-decoration-none btn btn-outline-light align-top mt-0 ml-1">Kusuma Yuda</a>
                        <br>
                        <i class="fab fa-instagram mt-3" style="font-size:2rem;"></i>
                        <a href="https://www.instagram.com/yuda_kusuma_25" class="text-white text-decoration-none btn btn-outline-light align-top mt-3 ml-1">yuda_kusuma_25</a>
                    </div>
                        <p class="copyright">&copy; copyright:2k20 Kusuma Yuda Mubarok.<br>SMK MUHAMMADIYAH 04 SUKOREJO.</p>
                </div>
            </section>
        </div>
    </div>
</div>
<!-- Optional JavaScript; choose one of the two! -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<!-- Option 2: jQuery, Popper.js, and Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
<!-- agar dtabel=jadi ada fareasinya -->
<script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<!-- jquary -->
<script>
    $(document).ready(function() {
    $('.dtabel').DataTable();
    } );
</script>
<script>  
    function runPopup(){
    window.alert("ANDA BERHASIL MENGHAPUS");
    };
</script>
</body>
</html>