<?php
include "connection.php";
include "function.php";

$toko=$db->query("select * from toko where id=".$_GET['id']);

$data_toko=$toko->fetchAll();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        body{
            font-family: sans-serif; 
            background-color: #f1c40f;
	        -webkit-animation: color 5s ease-in  0s infinite alternate running;
	        -moz-animation: color 5s linear  0s infinite alternate running;
	        animation: color 5s linear  0s infinite alternate running;
        }
        /* Animasi + Prefix untuk browser */
        @-webkit-keyframes color {
            12% { background-color: #00ff00; }
            25% { background-color: #b039bb; }
            50% { background-color: #4a4ad6; }
            75% { background-color: #3af50b; }
            100% { background-color: #f50b0b; }
        };
        @-moz-keyframes color {
            12% { background-color: #00ff00; }
            25% { background-color: #b039bb; }
            50% { background-color: #4a4ad6; }
            75% { background-color: #3af50b; }
            100% { background-color: #f50b0b; }
        };
        @keyframes color {
            12% { background-color: #00ff00; }
            25% { background-color: #b039bb; }
            50% { background-color: #4a4ad6; }
            75% { background-color: #3af50b; }
            100% { background-color: #f50b0b; }
        };
    </style>
    <link rel="shortcut icon" type="image/png" href="img/logo.png"/>
    <title>♛ʏᴜᴅᴀ ɢᴀᴍɪɴɢ sᴛᴏʀᴇ♛</title>
</head>
<body>
    <!-- EDIT -->
<div class="container">
    <div class="row">
        <div class="col">
        <form action="index.php" method="POST">
           <center><h3><b>꧁EDIT YOUR POST ITEM HERE꧂</b></h3></center>
            <div class="form-group">
                <label for="exampleInputEmail1">Brand</label>
                <input type="text" name="nama" class="form-control" value="<?php echo $data_toko[0]['nama'] ?>" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">From City</label>
                <input type="text" name="asal" class="form-control" value="<?php echo $data_toko[0]['asal'] ?>" id="exampleInputPassword1">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Price</label>
                <input type="text" name="harga" class="form-control" value="<?php echo $data_toko[0]['harga'] ?>" id="exampleInputPassword1">
            </div>
            <input type="hidden" name="id" value="<?php echo $data_toko[0]['id']; ?>">
            <button onclick="runPopup()" type="submit" name="ganti" class="btn btn-success">SAVE <i class="far fa-save"></i></button>
        </form>
        </div>
    </div>
</div>
<script>  
    function runPopup(){
        window.alert("ANDA BERHASIL MENGEDIT ITEM ANDA");
    };
</script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>